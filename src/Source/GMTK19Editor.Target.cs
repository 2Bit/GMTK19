// Copyright 2019 2Bit Studios, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class GMTK19EditorTarget : TargetRules
{
	public GMTK19EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "GMTK19" } );
	}
}
