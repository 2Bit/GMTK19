// Copyright 2019 2Bit Studios, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class GMTK19Target : TargetRules
{
	public GMTK19Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "GMTK19" } );
	}
}
