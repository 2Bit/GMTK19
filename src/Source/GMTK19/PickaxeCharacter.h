// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "PickaxeCharacter.generated.h"

UCLASS()
class APickaxeCharacter : public ACharacter
{
	GENERATED_BODY()

	APickaxeCharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = true))
	class USoundWave* HitSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Audio, meta = (AllowPrivateAccess = true))
	class USoundWave* MissSound;

public:
	UFUNCTION(BlueprintCallable, Category = Data)
	void Configure(class UPickaxe* NewTarget);

	UFUNCTION(BlueprintCallable, Category = Animation)
	void PlayAttack(APickaxeCharacter* NewAttackTarget, bool DoesAttackHit);

	UFUNCTION(BlueprintCallable, Category = Animation)
	void PlayHurt();

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UCameraShake> HitShakeClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = true))
	class UPickaxe* Target;

	UPROPERTY()
	float Randomness;

	UPROPERTY()
	APickaxeCharacter* AttackTarget;

	UPROPERTY()
	float AttackTimeRemaining;

	UPROPERTY()
	float AttackRandomness;

	UPROPERTY()
	uint8 bDoesAttackHit : 1;

	UPROPERTY()
	float RecoilTimeRemaining;

	UPROPERTY()
	class UMaterialInstanceDynamic* BodyMaterial;

	UPROPERTY()
	class UMaterialInstanceDynamic* HeadMaterial;

public:
	FORCEINLINE class UPickaxe* GetTarget() const { return this->Target; }
};
