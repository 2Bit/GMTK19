// Copyright 2019 2Bit Studios, All Rights Reserved.


#include "BreedingBlueprintFunctionLibrary.h"

#include "Kismet/KismetArrayLibrary.h"
#include "Pickaxe.h"
#include "OreStruct.h"
#include "Inventory.h"

//static TArray<FString> prefixes = {"Aggravated", "Endowed", "Empowered", "Elegant", "Quick"};
//static TArray<FString> suffixes = {"Aggravation", "Endowment", "Empowerment", "Elegance", "Quickness"};
static TArray<FString> affixes = 
{ "Smash", "Bash", "Blast", "Bang", "Blow", "Boom", "Burst", "Doom", "Calamity", 
"Annihilation", "Dark", "Dusk", "Shade", "Gloom", "Murk", "Night", "Helen", "Sidney", 
"Meme", "Dank", "Stink", "Hate", "Pain", "Revenge", "Venom", "Murder", "Crime", "Danger", 
"Menace", "Peril", "Crisis", "Risk" };

UPickaxe* UBreedingBlueprintFunctionLibrary::Smelt(UPickaxe* A, UPickaxe* B, const FOreStruct oreCounts, UInventory* const playerInventory)
{
    if(playerInventory == nullptr)
    {
        UE_LOG(LogTemp, Error, TEXT("Whoops"));
        return nullptr;
    };

    FPickaxeData DataParentA = A->GetData();
    FPickaxeData DataParentB = B->GetData();

    FPickaxeData DataChild;

    //Generate Child Values
    Heuristic(DataParentA.Yield, DataParentB.Yield, &DataChild.Yield);
    
    Heuristic(DataParentA.Durability, DataParentB.Durability, &DataChild.Durability);
    
    Heuristic(DataParentA.Attack, DataParentB.Attack, &DataChild.Attack);
    
    Heuristic(DataParentA.Defense, DataParentB.Defense, &DataChild.Defense);

    DataChild.Yield.Value += 0.25f * oreCounts.Yield; 
    DataChild.Durability.Value += 0.25f * oreCounts.Durability; 
    DataChild.Attack.Value += 0.25f * oreCounts.Attack; 
    DataChild.Defense.Value += 0.25f * oreCounts.Defense; 

    DataChild.Name = GetName(DataChild);
	DataChild.Colour = FLinearColor(FMath::RandRange(0.0f, 0.6f), FMath::RandRange(0.0f, 0.6f), FMath::RandRange(0.0f, 0.6f));

    TArray<EPickaxeAction> ParentActions = {DataParentA.Action1, DataParentB.Action1, DataParentA.Action2, DataParentB.Action2};

    Shuffle<EPickaxeAction>(ParentActions);

    DataChild.Action1 = ParentActions[0];
    DataChild.Action2 = EPickaxeAction::None;
    for(int i = 1; i < 4 && (DataChild.Action2 == DataChild.Action1 || DataChild.Action2 == EPickaxeAction::None); i++){
        DataChild.Action2 = ParentActions[i];
    }
    
    UPickaxe* ChildAxe = NewObject<UPickaxe>(playerInventory);
    ChildAxe->SetData(DataChild);

    playerInventory->AddPickaxe(ChildAxe);
    return playerInventory->GetPickaxe(0);
}

UPickaxe* UBreedingBlueprintFunctionLibrary::Summon(UObject* outer, const FPickaxeData& data)
{

    FPickaxeData DataSummon;

    //Generate summon values
    Heuristic(data.Yield, data.Yield, &DataSummon.Yield);
    
    Heuristic(data.Durability, data.Durability, &DataSummon.Durability);
    
    Heuristic(data.Attack, data.Attack, &DataSummon.Attack);
    
    Heuristic(data.Defense, data.Defense, &DataSummon.Defense);

    DataSummon.Name = GetName(DataSummon);

    // Roll Abilities
    float chanceRare = FMath::RandRange(0.0001f, 0.9999f);
    if(chanceRare >= 0.9f){
        DataSummon.Action1 = UPickaxeActionHelper::RarePool[FMath::RandRange(0, UPickaxeActionHelper::RarePool.Num() - 1)];
    }
    else {
        DataSummon.Action1 = UPickaxeActionHelper::CommonPool[FMath::RandRange(0, UPickaxeActionHelper::CommonPool.Num() - 1)];
    }
    
    DataSummon.Action2 = EPickaxeAction::None;

    while(DataSummon.Action2 == EPickaxeAction::None || DataSummon.Action2 == DataSummon.Action1)
    {
        if(chanceRare <= 0.10f){
            DataSummon.Action2 = UPickaxeActionHelper::RarePool[FMath::RandRange(0, UPickaxeActionHelper::RarePool.Num() - 1)];
        }
        else {
            DataSummon.Action2 = UPickaxeActionHelper::CommonPool[FMath::RandRange(0, UPickaxeActionHelper::CommonPool.Num() - 1)];
        }
    }
    
    UPickaxe* summoned = NewObject<UPickaxe>(outer);
    summoned->SetData(DataSummon);

    return summoned;
}

template<typename T>
void UBreedingBlueprintFunctionLibrary::Shuffle(TArray<T> TargetArray)
{
	if (TargetArray.Num() > 0)
	{
		int32 LastIndex = TargetArray.Num() - 1;
		for (int32 i = 0; i <= LastIndex; ++i)
		{
			int32 Index = FMath::RandRange(i, LastIndex);
			if (i != Index)
			{
				TargetArray.Swap(i, Index);
			}
		}
	}
}

void  UBreedingBlueprintFunctionLibrary::Heuristic(const FStatistic& A, const FStatistic& B, FStatistic* Child)
{
    float mean = FMath::Max(A.Value, B.Value) + FMath::RandRange(-1.0f, 3.0f);
    float std = (A.STD + B.STD) / 2.0 + FMath::Exp(0.01f*(mean - FMath::Min(A.Value, B.Value)));
    *Child = FStatistic(FMath::Max(mean + std * GaussRandom(), 0.0f), FMath::Max(std, 0.01f));
}

float UBreedingBlueprintFunctionLibrary::GaussRandom() {
    float u = FMath::RandRange(-1.0f, 1.0f);
    float v = FMath::RandRange(-1.0f, 1.0f);
    float r = u*u + v*v;
    /*if outside interval [0,1] start over*/
    if(r == 0 || r >= 1) return GaussRandom();

    float c = FMath::Sqrt(-2*FMath::Loge(r)/r);
    return u*c;
}

FStatistic UBreedingBlueprintFunctionLibrary::GetForgeValue(const float& value, const float& total)
{
    return FStatistic(value + FMath::RandRange(0,3), FMath::Max((1.0f - value / total + FMath::RandRange(-0.05f, 0.05)) * FMath::Max(value, 0.5f), 0.01f));
}

UPickaxe* UBreedingBlueprintFunctionLibrary::Forge(const FOreStruct oreCounts, UInventory* const playerInventory)
{
    if(playerInventory == nullptr)
    {
        UE_LOG(LogTemp, Error, TEXT("Whoops"));
        return nullptr;
    };

    FPickaxeData NewData;

    // Roll Stats
    float total = oreCounts.Yield + oreCounts.Durability + oreCounts.Attack + oreCounts.Defense;

    NewData.Yield = GetForgeValue(oreCounts.Yield, total);
    
    NewData.Durability = GetForgeValue(oreCounts.Durability, total);
    
    NewData.Attack = GetForgeValue(oreCounts.Attack, total);
    
    NewData.Defense = GetForgeValue(oreCounts.Defense, total);

    // Add Defaults
    NewData.Yield.Value += 1;    
    NewData.Durability.Value += 10;
    NewData.Attack.Value += 5;
    NewData.Defense.Value += 5;

    // Roll Abilities
    float chanceRare = FMath::RandRange(0.0001f, 0.9999f);
    if(chanceRare >= FMath::Min(1.0f - total/1000.0f, 0.55f)){
        NewData.Action1 = UPickaxeActionHelper::RarePool[FMath::RandRange(0, UPickaxeActionHelper::RarePool.Num() - 1)];
    }
    else {
        NewData.Action1 = UPickaxeActionHelper::CommonPool[FMath::RandRange(0, UPickaxeActionHelper::CommonPool.Num() - 1)];
    }
    
    NewData.Action2 = EPickaxeAction::None;

    while(NewData.Action2 == EPickaxeAction::None || NewData.Action2 == NewData.Action1)
    {
        if(chanceRare <= FMath::Min(total/1000.0f, 0.45f)){
            NewData.Action2 = UPickaxeActionHelper::RarePool[FMath::RandRange(0, UPickaxeActionHelper::RarePool.Num() - 1)];
        }
        else {
            NewData.Action2 = UPickaxeActionHelper::CommonPool[FMath::RandRange(0, UPickaxeActionHelper::CommonPool.Num() - 1)];
        }
    }

    // Roll Name
    NewData.Name = GetName(NewData);
	NewData.Colour = FLinearColor(FMath::RandRange(0.0f, 0.6f), FMath::RandRange(0.0f, 0.6f), FMath::RandRange(0.0f, 0.6f));

    // Attach to object and return
    UPickaxe* NewAxe = NewObject<UPickaxe>(playerInventory);    
    NewAxe->SetData(NewData);
    playerInventory->AddPickaxe(NewAxe);  
    return playerInventory->GetPickaxe(0);
}

FString UBreedingBlueprintFunctionLibrary::GetName(const FPickaxeData& data)
{
    float max = 0;
    FString typeName("");

    if(data.Yield.Value > max){
        max = data.Yield.Value;
        typeName = " Gold";
    }

    if(data.Durability.Value > max){
        max = data.Durability.Value;
        typeName = " Tugstan";
    }
    
    if(data.Attack.Value > max){
        max = data.Attack.Value;
        typeName = " Diamond";
    }    
    
    if(data.Defense.Value > max){
        max = data.Defense.Value;
        typeName = " Iron";
    }

    //return typeName + FString(" Pickaxe ") + affixes[FMath::RandRange(1, affixes.Num())-1] + affixes[FMath::RandRange(1, affixes.Num())-1];
    return affixes[FMath::RandRange(1, affixes.Num())-1] + affixes[FMath::RandRange(1, affixes.Num())-1].ToLower();
}