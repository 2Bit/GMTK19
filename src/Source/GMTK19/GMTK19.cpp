// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "GMTK19.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, GMTK19, "GMTK19");
