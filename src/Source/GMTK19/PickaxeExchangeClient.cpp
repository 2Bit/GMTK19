#include "PickaxeExchangeClient.h"

APickaxeExchangeClient::APickaxeExchangeClient(){ PrimaryActorTick.bCanEverTick = false; }
void APickaxeExchangeClient::BeginPlay() { 
	Super::BeginPlay(); 
	Http = &FHttpModule::Get(); 
}

TSharedRef<IHttpRequest> APickaxeExchangeClient::RequestWithRoute(FString Subroute) {
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	FString BaseURL = this->HostOverride.IsEmpty() ? (bUseLocal ? LocalHost : RemoteHost) : this->HostOverride;
	Request->SetURL(BaseURL + Subroute);
	SetRequestHeaders(Request);
	return Request;
}

void APickaxeExchangeClient::SetRequestHeaders(TSharedRef<IHttpRequest>& Request) {
	Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));
	Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
	Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
}

TSharedRef<IHttpRequest> APickaxeExchangeClient::GetRequest(FString Subroute, const TMap<FString, FString>& QueryParams) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("GET");

	bool bStart = true;
	FString QueryString = FString("");
	for(const auto& Param : QueryParams)
	{
		QueryString += (bStart ? "?" : "&") + Param.Key + "=" + Param.Value;
		bStart = false;
	}

	Request->SetURL(Request->GetURL() + QueryString);

	return Request;
}

TSharedRef<IHttpRequest> APickaxeExchangeClient::PostRequest(FString Subroute, FString ContentJsonString) {
	TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
	Request->SetVerb("POST");
	Request->SetContentAsString(ContentJsonString);
	return Request;
}

void APickaxeExchangeClient::Send(TSharedRef<IHttpRequest>& Request) {
	Request->ProcessRequest();
}

bool UPickaxeExchangeRequestWrapper::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful) {
	if (!bWasSuccessful || !Response.IsValid()) return false;
	if (EHttpResponseCodes::IsOk(Response->GetResponseCode())) return true;
	else {
		UE_LOG(LogTemp, Warning, TEXT("Http Response returned error code: %d"), Response->GetResponseCode());
		return false;
	}
}

void APickaxeExchangeClient::GetPlayerData(FString Name, const FGetPlayerDataCallback& Callback)
{
	TMap<FString, FString> QueryParams;
	QueryParams.Add(FString("name"), Name);
	
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->GetPlayerDataCallback = Callback;

	TSharedRef<IHttpRequest> Request = GetRequest("get-player-data", QueryParams);
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::GetPlayerDataResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::AddPlayer(FClientPlayerInfo NewPlayer, const FAddPlayerCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->AddPlayerCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("add-player", NewPlayer.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::AddPlayerResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::SetResource(FResourceRequest SetResourceRequest, const FSetResourceCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->SetResourceCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("set-resource", SetResourceRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::SetResourceResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::PostListing(FPostListingRequest PostListingRequest, const FPostListingCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->PostListingCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("post-listing", PostListingRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::PostListingResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::ClaimListing(FClaimListingRequest ClaimListingRequest, const FClaimListingCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->ClaimListingCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("claim-listing", ClaimListingRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::ClaimListingResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::RemoveListing(FClaimListingRequest RemoveListingRequest, const FClaimListingCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->ClaimListingCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("remove-listing", RemoveListingRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::ClaimListingResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::RemovePickaxe(FRemovePickaxeRequest RemovePickaxeRequest, const FRemovePickaxeCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->RemovePickaxeCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("remove-pickaxe", RemovePickaxeRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::RemovePickaxeResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::Purchase(FPurchaseRequest PurchaseRequest, const FPurchaseCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->PurchaseCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("purchase", PurchaseRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::PurchaseResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::GetListings(FGetListingRequest GetListingsRequest, const FGetListingsCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->GetListingsCallback = Callback;

	TSharedRef<IHttpRequest> Request = GetRequest("get-listings", GetListingsRequest.ToQuery());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::GetListingsResponse);
	Request->ProcessRequest();
}

void APickaxeExchangeClient::AddPickaxe(FAddPickaxeRequest AddPickaxeRequest, const FAddPickaxeCallback& Callback)
{
	auto* RequestWrapper = NewObject<UPickaxeExchangeRequestWrapper>(this, NAME_None, EObjectFlags::RF_Transient);
	RequestWrapper->AddToRoot();
	RequestWrapper->AddPickaxeCallback = Callback;

	TSharedRef<IHttpRequest> Request = PostRequest("add-pickaxe", AddPickaxeRequest.ToJson());
	Request->OnProcessRequestComplete().BindUObject(RequestWrapper, &UPickaxeExchangeRequestWrapper::AddPickaxeResponse);
	Request->ProcessRequest();
}

void UPickaxeExchangeRequestWrapper::GetPlayerDataResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->GetPlayerDataCallback.ExecuteIfBound(FClientPlayerData(), false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->GetPlayerDataCallback.ExecuteIfBound(FClientPlayerData(), false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->GetPlayerDataCallback.ExecuteIfBound(FClientPlayerData(JsonObject), true);
	}
}

void UPickaxeExchangeRequestWrapper::AddPlayerResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->AddPlayerCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->AddPlayerCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->AddPlayerCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::SetResourceResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->SetResourceCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->SetResourceCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->SetResourceCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::PostListingResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->PostListingCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->PostListingCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->PostListingCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::ClaimListingResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->ClaimListingCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->ClaimListingCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->ClaimListingCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::AddPickaxeResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->AddPickaxeCallback.ExecuteIfBound(0, false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->AddPickaxeCallback.ExecuteIfBound(0, false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->AddPickaxeCallback.ExecuteIfBound(JsonObject->GetIntegerField("axe_id"), true);
	}
}

void UPickaxeExchangeRequestWrapper::RemovePickaxeResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->RemovePickaxeCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->RemovePickaxeCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->RemovePickaxeCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::PurchaseResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->PurchaseCallback.ExecuteIfBound(false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->PurchaseCallback.ExecuteIfBound(false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		this->PurchaseCallback.ExecuteIfBound(true);
	}
}

void UPickaxeExchangeRequestWrapper::GetListingsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	this->RemoveFromRoot();
	if (ResponseIsValid(Response, bWasSuccessful)) 
	{
		FString JsonString = Response->GetContentAsString();
		TSharedPtr<FJsonObject> JsonObject;
		TSharedRef<TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);
		if (!FJsonSerializer::Deserialize(JsonReader, JsonObject) || !JsonObject.IsValid())
		{
			UE_LOG(LogJson, Warning, TEXT("JsonObjectStringToUStruct - Unable to parse json=[%s]"), *JsonString);
			this->GetListingsCallback.ExecuteIfBound(TArray<FExchangeListing>(), false);
			return;
		}

		if(JsonObject->HasField("error")){
			this->GetListingsCallback.ExecuteIfBound(TArray<FExchangeListing>(), false);
			UE_LOG(LogJson, Error, TEXT("SQL Error: [%s]"), *JsonObject->GetStringField("error"));
			return;
		}

		TArray<FExchangeListing> Listings;
		for(auto& Listing : JsonObject->GetArrayField("listings"))
		{
			Listings.Add(FExchangeListing(Listing->AsObject()));
		}

		this->GetListingsCallback.ExecuteIfBound(Listings, true);
	}
}
