// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Distributions/DistributionFloatUniformCurve.h"
#include "Statistic.generated.h"

USTRUCT(BlueprintType)
struct FStatistic
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Statistic)
	float Value;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Statistic)
	float STD;

	FStatistic(){ this->Value = 5; this->STD = 2.5;}

	FStatistic(float value, float std){ this->Value = value; this->STD = std;}
};