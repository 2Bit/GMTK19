// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "OreStruct.generated.h"

USTRUCT(BlueprintType)
struct FOreStruct
{
	GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OreStruct)
    int32 Yield;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OreStruct)
    int32 Durability;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OreStruct)
    int32 Attack;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OreStruct)
    int32 Defense;

    FOreStruct() : Yield(0), Durability(0), Attack(0), Defense(0) {};

    FOreStruct(int32 yield, int32 durability, int32 attack, int32 defence) : Yield(yield), Durability(durability), Attack(attack), Defense(defence) {};

    FOreStruct& operator+=(const FOreStruct& rhs){
        this->Yield += rhs.Yield;
        this->Durability += rhs.Durability;
        this->Attack += rhs.Attack;
        this->Defense += rhs.Defense;
        return *this;
    }

    FOreStruct& operator-=(const FOreStruct& rhs){
        this->Yield -= rhs.Yield;
        this->Durability -= rhs.Durability;
        this->Attack -= rhs.Attack;
        this->Defense -= rhs.Defense;
        return *this;
    }
};