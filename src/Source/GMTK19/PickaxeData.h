// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "PickaxeAction.h"
#include "Statistic.h"
#include "Json.h"
#include "JsonUtilities.h"

#include "PickaxeData.generated.h"

USTRUCT(BlueprintType)
struct FPickaxeData
{
	GENERATED_BODY()

	FPickaxeData() {}
	FPickaxeData(TSharedPtr<FJsonObject> JsonObject)
	{
		ID = JsonObject->GetIntegerField("axe_id");
		Name = JsonObject->GetStringField("name");
		Yield = FStatistic(JsonObject->GetNumberField("yield_value"), JsonObject->GetNumberField("yield_std"));
		Durability = FStatistic(JsonObject->GetNumberField("durability_value"), JsonObject->GetNumberField("durability_std"));
		Attack = FStatistic(JsonObject->GetNumberField("attack_value"), JsonObject->GetNumberField("attack_std"));
		Defense = FStatistic(JsonObject->GetNumberField("defense_value"), JsonObject->GetNumberField("defense_std"));
		Action1 = (EPickaxeAction)JsonObject->GetIntegerField("action1");
		Action2 = (EPickaxeAction)JsonObject->GetIntegerField("action2");
	}

	FString ToJson()
	{
		return "\"axe_id\": " + FString::FromInt(ID) 
		+ ", \"name\": \"" + Name 
		+ "\", \"yield_value\": " + FString::SanitizeFloat(Yield.Value) + ", \"yield_std\": " + FString::SanitizeFloat(Yield.STD)
		+ ", \"durability_value\": " + FString::SanitizeFloat(Durability.Value) + ", \"durability_std\": " + FString::SanitizeFloat(Durability.STD) 
		+ ", \"attack_value\": " + FString::SanitizeFloat(Attack.Value) + ", \"attack_std\": " + FString::SanitizeFloat(Attack.STD) 
		+ ", \"defense_value\": " + FString::SanitizeFloat(Defense.Value) + ", \"defense_std\": " + FString::SanitizeFloat(Defense.STD) 
		+ ", \"action1\": " + FString::FromInt((int)Action1)
		+ ", \"action2\": " + FString::FromInt((int)Action2);
	}

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	int ID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	FLinearColor Colour;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FStatistic Yield;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FStatistic Durability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FStatistic Attack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attributes)
	FStatistic Defense;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Actions)
	EPickaxeAction Action1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Actions)
	EPickaxeAction Action2;
};
