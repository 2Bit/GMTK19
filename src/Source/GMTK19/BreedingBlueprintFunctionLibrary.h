// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "Statistic.h"
#include "OreStruct.h"
#include "PickaxeData.h"

#include "BreedingBlueprintFunctionLibrary.generated.h"


/**
 * 
 */
UCLASS()
class GMTK19_API UBreedingBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = Breeding)
	static UPickaxe* Smelt(UPickaxe* A, UPickaxe* B, const FOreStruct oreCounts, UInventory* const playerInventory);

	UFUNCTION(BlueprintCallable, Category = Breeding)
	static UPickaxe* Forge(const FOreStruct oreCounts, UInventory* const playerInventory);

	UFUNCTION(BlueprintCallable, Category = LifeMagic)
	static UPickaxe* Summon(UObject* outer, const FPickaxeData& data);

    UFUNCTION(BlueprintCallable, BlueprintPure, Category = OreStruct)
    static void Invert(FOreStruct& in, UPARAM(ref) FOreStruct& out)
    {
        out.Yield = -in.Yield;
        out.Durability = -in.Durability;
        out.Attack = -in.Attack;
        out.Defense = -in.Defense;
    }

	static FString GetName(const FPickaxeData& data);

private:
	static void Heuristic(const FStatistic& A, const FStatistic& B, FStatistic* Child);
	static float GaussRandom();
	template<typename T> static void Shuffle(TArray<T> TargetArray);
	static FStatistic GetForgeValue(const float& value, const float& total);
};
