// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PickaxeWidget.generated.h"

/**
 * 
 */
UCLASS()
class GMTK19_API UPickaxeWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Data)
	void Configure(class UPickaxe* Player);
};
