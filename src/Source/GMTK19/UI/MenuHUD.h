// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HUDWidget.h"

#include "MenuHUD.generated.h"

UCLASS()
class GMTK19_API UMenuHUD : public UHUDWidget
{
	GENERATED_BODY()
};
