// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HUDWidget.h"

#include "BreedingHUD.generated.h"

/**
 * UI for breeding, blueprint has the actual layout.
 * This is just so code can call the UI if needed.
 */
UCLASS()
class GMTK19_API UBreedingHUD : public UHUDWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Configure(class UInventory* Inventory);

};
