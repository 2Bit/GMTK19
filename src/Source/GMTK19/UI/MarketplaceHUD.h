// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HUDWidget.h"

#include "MarketplaceHUD.generated.h"

UCLASS()
class GMTK19_API UMarketplaceHUD : public UHUDWidget
{
	GENERATED_BODY()
};
