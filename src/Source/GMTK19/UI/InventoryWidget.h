// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "InventoryWidget.generated.h"

UCLASS()
class UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Configure(class UInventory* Inventory);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Refresh();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	bool IsShowing() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Show(FString const& RetrieveText, bool selectable);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Hide();
};
