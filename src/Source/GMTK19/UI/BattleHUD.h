// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "HUDWidget.h"

#include "PickaxeAction.h"

#include "BattleHUD.generated.h"

UCLASS()
class UBattleHUD : public UHUDWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void Init(class APickaxeCharacter* NewPlayerCharacter, class APickaxeCharacter* NewEnemyCharacter);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void Configure(class UPickaxe* Player, class UPickaxe* Enemy);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	class UPickaxe* GetPlayer() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	class UPickaxe* GetEnemy() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	bool IsEnemyTurn() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void EndPlayerTurn();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void EndEnemyTurn();

	UFUNCTION(BlueprintCallable)
	bool PerformAction(class UPickaxe* Origin, class UPickaxe* Target, EPickaxeAction Action);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void ShowMessage(FString const& MessageText);

	UFUNCTION(BlueprintCallable)
	void ShowHitMessage(class UPickaxe* Origin, EPickaxeAction Action, int32 Damage);

	UFUNCTION(BlueprintCallable)
	void ShowMissMessage(class UPickaxe* Origin, EPickaxeAction Action);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class APickaxeCharacter* PlayerCharacter;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = true))
	class APickaxeCharacter* EnemyCharacter;
};
