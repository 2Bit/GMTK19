// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "BattleHUD.h"

#include "PickaxeCharacter.h"
#include "Pickaxe.h"

void UBattleHUD::Init(APickaxeCharacter* NewPlayerCharacter, APickaxeCharacter* NewEnemyCharacter)
{
	this->PlayerCharacter = NewPlayerCharacter;
	this->EnemyCharacter = NewEnemyCharacter;
}

bool UBattleHUD::PerformAction(UPickaxe* Origin, UPickaxe* Target, EPickaxeAction Action)
{
	FPickaxeActionDetails const ActionDetails = UPickaxeActionHelper::GetDetails(Action);

	bool DoesAttackHit = true;
	if (FMath::FRand() <= ActionDetails.Accuracy)
	{
		int32 const Damage = Target->CalculateIncomingDamage(Origin->CalculateOutgoingDamage(Action));

		this->ShowHitMessage(Origin, Action, Damage);

		Target->ModifyHitPoints(-Damage);
	}
	else
	{
		this->ShowMissMessage(Origin, Action);

		DoesAttackHit = false;
	}

	if (this->PlayerCharacter->GetTarget() == Origin)
	{
		if (this->EnemyCharacter->GetTarget() == Target)
		{
			this->PlayerCharacter->PlayAttack(this->EnemyCharacter, DoesAttackHit);
		}
	}
	else if (this->EnemyCharacter->GetTarget() == Origin)
	{
		if (this->PlayerCharacter->GetTarget() == Target)
		{
			this->EnemyCharacter->PlayAttack(this->PlayerCharacter, DoesAttackHit);
		}
	}

	return true;
}

void UBattleHUD::ShowHitMessage(UPickaxe* Origin, EPickaxeAction Action, int32 Damage)
{
	this->ShowMessage(Origin->GetData().Name.ToUpper() + " uses " + UPickaxeActionHelper::GetDetails(Action).Name.ToUpper() + "! It does " + FString::FromInt(Damage) + " damage!");
}

void UBattleHUD::ShowMissMessage(UPickaxe* Origin, EPickaxeAction Action)
{
	this->ShowMessage(Origin->GetData().Name.ToUpper() + " uses " + UPickaxeActionHelper::GetDetails(Action).Name.ToUpper() + "! It misses.");
}
