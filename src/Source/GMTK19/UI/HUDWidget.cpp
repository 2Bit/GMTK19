// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "HUDWidget.h"

#include "WidgetAnimation.h"

void UHUDWidget::NativeConstruct()
{
	Super::NativeConstruct();

	this->PlayAnimation(this->GetShowAnimation(), 0.0f, 1, EUMGSequencePlayMode::Reverse, 10000.0f);
	this->SetVisibility(ESlateVisibility::HitTestInvisible);
}

void UHUDWidget::Show()
{
	if (!this->bIsShowing)
	{
		this->PlayAnimationForward(this->GetShowAnimation());
		this->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

		this->bIsShowing = true;
	}
}

void UHUDWidget::Hide()
{
	if (this->bIsShowing)
	{
		this->PlayAnimationReverse(this->GetShowAnimation());
		this->SetVisibility(ESlateVisibility::HitTestInvisible);

		this->bIsShowing = false;
	}
}
