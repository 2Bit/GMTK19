// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "OreSelectionHUD.generated.h"

/**
 * 
 */
UCLASS()
class GMTK19_API UOreSelectionHUD : public UUserWidget
{
	GENERATED_BODY()
	
};
