// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LoginHUD.generated.h"

/**
 * 
 */
UCLASS()
class GMTK19_API ULoginHUD : public UUserWidget
{
	GENERATED_BODY()
	
};
