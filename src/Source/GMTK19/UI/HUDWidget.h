// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "HUDWidget.generated.h"

UCLASS()
class UHUDWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativeConstruct() override;

public:
	UFUNCTION(BlueprintCallable, Category = Visibility)
	void Show();

	UFUNCTION(BlueprintCallable, Category = Visibility)
	void Hide();

protected:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Visibility)
	class UWidgetAnimation* GetShowAnimation() const;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Visibility, meta = (AllowPrivateAccess = true))
	uint8 bIsShowing : 1;

public:
	FORCEINLINE bool IsShowing() const { return this->bIsShowing; }
};
