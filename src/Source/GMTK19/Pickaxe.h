// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "PickaxeData.h"

#include "Pickaxe.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPickaxeStatusChange, UPickaxe*, Origin);

UCLASS(BlueprintType)
class UPickaxe : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = Status)
	FPickaxeStatusChange OnStatusChange;

	UFUNCTION(BlueprintCallable, Category = Data)
	void SetData(FPickaxeData const& NewData);

	UFUNCTION(BlueprintCallable, Category = Status)
	void ModifyHitPoints(int32 DeltaHitPoints);

	UFUNCTION(BlueprintCallable, Category = Status)
	void BeginDefending();

	UFUNCTION(BlueprintCallable, Category = Status)
	void EndDefending();

	UFUNCTION(BlueprintCallable, Category = Status)
	int32 CalculateOutgoingDamage(EPickaxeAction Action) const;

	UFUNCTION(BlueprintCallable, Category = Status)
	int32 CalculateIncomingDamage(int32 RawDamage) const;
	
	UFUNCTION(BlueprintCallable, Category = Status)
	FString GetName() {return this->Data.Name;}

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = true))
	FPickaxeData Data;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status, meta = (AllowPrivateAccess = true))
	int32 HitPoints;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status, meta = (AllowPrivateAccess = true))
	uint8 bIsDead : 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status, meta = (AllowPrivateAccess = true))
	uint8 bIsDefending : 1;

public:
	FORCEINLINE FPickaxeData* GetDataPtr() { return &this->Data; }
	FORCEINLINE FPickaxeData GetData() const { return this->Data; }
	FORCEINLINE int32 GetHitPoints() const { return this->HitPoints; }
	FORCEINLINE bool IsDead() const { return this->bIsDead; }
	FORCEINLINE bool IsDefending() const { return this->bIsDefending; }
};
