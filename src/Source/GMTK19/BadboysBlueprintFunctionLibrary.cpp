// Copyright 2019 2Bit Studios, All Rights Reserved.


#include "BadboysBlueprintFunctionLibrary.h"

int32 UBadboysBlueprintFunctionLibrary::GetUniqueID(const UObject* Object)
{
	return Object->GetUniqueID();
}
