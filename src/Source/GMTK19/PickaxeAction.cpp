// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "PickaxeAction.h"

TArray<EPickaxeAction> UPickaxeActionHelper::CommonPool = {EPickaxeAction::Smash, EPickaxeAction::Pry, EPickaxeAction::Whack, EPickaxeAction::Slam, EPickaxeAction::Poke };
TArray<EPickaxeAction> UPickaxeActionHelper::RarePool = { EPickaxeAction::MegaSmash, EPickaxeAction::Swipe };

FPickaxeActionDetails UPickaxeActionHelper::GetDetails(EPickaxeAction Action)
{
	FPickaxeActionDetails Details;
	Details.Name = "None";
	Details.Description = "Congratulations, this bug is actually an easter egg so don't report it to us.";
	Details.Damage = 0.0f;
	Details.Accuracy = 0.0f;

	switch (Action)
	{
	case EPickaxeAction::Smash:
		Details.Name = "Smash";
		Details.Description = "Nothing simpler than hitting something.";
		Details.Damage = 1.0f;
		Details.Accuracy = 0.7f;

		break;
	case EPickaxeAction::Pry:
		Details.Name = "Pry";
		Details.Description = "Make use of the often underappreciated back end of the pick.";
		Details.Damage = 0.8f;
		Details.Accuracy = 1.0f;

		break;
	case EPickaxeAction::MegaSmash:
		Details.Name = "MegaSmash";
		Details.Description = "It's like smash but MEGA!";
		Details.Damage = 3.0f;
		Details.Accuracy = 0.66f;

		break;
	case EPickaxeAction::Whack:
		Details.Name = "Whack";
		Details.Description = "Right to the pickteeth.";
		Details.Damage = 1.0f;
		Details.Accuracy = 0.75f;

		break;
	case EPickaxeAction::Slam:
		Details.Name = "Slam";
		Details.Description = "Slam.";
		Details.Damage = 1.8f;
		Details.Accuracy = 0.4f;

		break;
	case EPickaxeAction::Poke:
		Details.Name = "Poke";
		Details.Description = "Don't know how this works but it does.";
		Details.Damage = 0.9f;
		Details.Accuracy = 0.95f;

		break;
	case EPickaxeAction::Swipe:
		Details.Name = "Swipe";
		Details.Description = "Just their hit points, not their wallets.";
		Details.Damage = 2.0f;
		Details.Accuracy = 1.0f;

		break;
	}

	return Details;
}
