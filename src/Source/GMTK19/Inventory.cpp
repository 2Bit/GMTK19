// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "Inventory.h"

#include "Pickaxe.h"

int32 UInventory::GetPickaxeCount() const
{
	return this->Pickaxes.Num();
}

UPickaxe* UInventory::GetPickaxe(int32 Slot)
{
	if (Slot >= 0 && Slot < this->GetPickaxeCount())
	{
		return this->Pickaxes[Slot];
	}

	return nullptr;
}

bool UInventory::AddPickaxe(UPickaxe* NewPickaxe)
{
	if (NewPickaxe == nullptr)
	{
		return false;
	}

	bool const Result = this->Pickaxes.Insert(NewPickaxe, 0) >= 0;
	if (Result)
	{
		this->OnInventoryChanged.Broadcast();
		this->OnPickaxeAdded.Broadcast(NewPickaxe);
	}

	return Result;
}

UPickaxe* UInventory::RemovePickaxeAt(int32 Slot)
{
	if (Slot >= 0 && Slot < this->GetPickaxeCount())
	{
		UPickaxe* const Result = this->Pickaxes[Slot];

		this->Pickaxes.RemoveAt(Slot);

		this->OnInventoryChanged.Broadcast();
		this->OnPickaxeRemoved.Broadcast(Result->GetData().ID);

		return Result;
	}

	return nullptr;
}

bool UInventory::RemovePickaxe(UPickaxe* axe)
{
	if(this->Pickaxes.Num() > 0 && axe)
	{
		this->OnPickaxeRemoved.Broadcast(axe->GetData().ID);
		return this->Pickaxes.Remove(axe) >= 0;
	}

	return false;
}

bool UInventory::RetrievePickaxe(int32 Slot)
{
	UPickaxe* const Result = this->RemovePickaxeAt(Slot);
	if (Result != nullptr)
	{
		this->OnRetrievePickaxe.Broadcast(Result);
		this->OnRetrievePickaxe.Clear();

		return true;
	}

	return false;
}

bool UInventory::IsLegitimateTransaction(const FOreStruct& Value)
{
	return (this->Ores.Yield + Value.Yield >= 0 &&
			this->Ores.Durability + Value.Durability >= 0 &&
			this->Ores.Attack + Value.Attack >= 0 &&
			this->Ores.Defense + Value.Defense >= 0);
}

bool UInventory::ModifyOreAmount(const FOreStruct& Value, bool isPositive)
{
	if (bool Result = IsLegitimateTransaction(Value))
	{
		if(isPositive){
			this->Ores += Value;
			}
		else{
			this->Ores -= Value;
		}
		this->OnInventoryChanged.Broadcast();

		return Result;
	}

	return false;
}
