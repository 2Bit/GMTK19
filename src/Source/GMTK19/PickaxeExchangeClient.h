// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PickaxeData.h"
#include "Ore.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "GameFramework/Actor.h"
#include "JsonUtilities.h"
#include "ExchangeListing.h"

#include "PickaxeExchangeClient.generated.h"

USTRUCT(BlueprintType)
struct FResourceRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EOre ResourceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int ResourceAmount;

	FString ToJson()
	{
		return "{\"name\":\"" + Name 
		+ "\",\"primary_resource_id\":" + FString::FromInt((int)ResourceType)
		+ ",\"resource_num\":" + FString::FromInt(ResourceAmount) + "}";
	}
};

USTRUCT(BlueprintType)
struct FPostListingRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int AxeID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EOre ListedResourceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int ListedResourceAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EOre BuyResourceType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int BuyResourceAmount;

	FString ToJson()
	{
		return "{\"name\":\"" + Name 
		+ "\",\"axe_id\":" + FString::FromInt(AxeID)
		+ ",\"listed_resource_id\":" + FString::FromInt((int)ListedResourceType)
		+ ",\"listed_resource_num\":" + FString::FromInt(ListedResourceAmount)
		+ ",\"buy_resource_id\":" + FString::FromInt((int)ListedResourceType)
		+ ",\"buy_resource_num\":" + FString::FromInt(ListedResourceAmount) 
		+ "}";
	}
};

USTRUCT(BlueprintType)
struct FClaimListingRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int ListingId;

	FString ToJson()
	{
		return "{\"listing_id\":" + FString::FromInt(ListingId) + "}";
	}
};

USTRUCT(BlueprintType)
struct FAddPickaxeRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString PlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FPickaxeData Axe;

	FString ToJson()
	{
		return "{\"name\":\"" + PlayerName
		+ "\",\"axe_name\":\"" + Axe.Name 
		+ "\",\"yield_value\":" + FString::SanitizeFloat(Axe.Yield.Value) + ",\"yield_std\":" + FString::SanitizeFloat(Axe.Yield.STD)
		+ ",\"durability_value\":" + FString::SanitizeFloat(Axe.Durability.Value) + ",\"durability_std\":" + FString::SanitizeFloat(Axe.Durability.STD) 
		+ ",\"attack_value\":" + FString::SanitizeFloat(Axe.Attack.Value) + ",\"attack_std\":" + FString::SanitizeFloat(Axe.Attack.STD) 
		+ ",\"defense_value\":" + FString::SanitizeFloat(Axe.Defense.Value) + ",\"defense_std\":" + FString::SanitizeFloat(Axe.Defense.STD) 
		+ ",\"action1\":" + FString::FromInt((int)Axe.Action1)
		+ ",\"action2\":" + FString::FromInt((int)Axe.Action2)
		+ "}";
	}
};

USTRUCT(BlueprintType)
struct FRemovePickaxeRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FPickaxeData Axe;

	FString ToJson()
	{
		return "{\"axe_id\":" + FString::FromInt(Axe.ID) + "}";
	}
};

USTRUCT(BlueprintType)
struct FPurchaseRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString PlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int ListingId;

	FString ToJson()
	{
		return "{\"listing_id\":" + FString::FromInt(ListingId) 
		+ ",\"name\":\"" + PlayerName 
		+ "\"}";
	}
};

USTRUCT(BlueprintType)
struct FGetListingRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString PlayerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int bGetPlayerPending;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int bGetClaimable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int ResourceFilterType;

	FString ToJson()
	{
		return "{\"filter_resource\":" + FString::FromInt(ResourceFilterType) 
		+ ",\"filter_by_seller_pending\":" + FString::FromInt(bGetPlayerPending)
		+ ",\"filter_by_seller_claimable\":" + FString::FromInt(bGetClaimable)
		+ ",\"seller_name\":\"" + PlayerName
		+ "\"}";
	}

	TMap<FString, FString> ToQuery()
	{
		TMap<FString, FString> Query;
		Query.Add("seller_name", PlayerName);
		Query.Add("filter_resource", FString::FromInt((int)ResourceFilterType));
		Query.Add("filter_by_seller_pending", FString::FromInt(bGetPlayerPending));
		Query.Add("filter_by_seller_claimable", FString::FromInt(bGetClaimable));

		return Query;
	}
};


USTRUCT(BlueprintType)
struct FClientPlayerInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EOre PrimaryOre;

	FString ToJson()
	{
		return "{\"name\":\"" + Name + "\",\"primary_resource_id\":" + FString::FromInt((int)PrimaryOre) + "}";
	}
};

USTRUCT(BlueprintType)
struct FClientPlayerData
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EOre PrimaryOre;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FPickaxeData> Pickaxes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TMap<EOre, int> Ores;

	FClientPlayerData() : Name(""), PrimaryOre((EOre)0), Pickaxes(TArray<FPickaxeData>()), Ores(TMap<EOre, int>()) {}

	FClientPlayerData(TSharedPtr<FJsonObject> JsonObject)
	{
		Name = JsonObject->GetStringField("player_hash");
		PrimaryOre = (EOre)JsonObject->GetIntegerField("primary_resource");
		for(auto& Pickaxe : JsonObject->GetArrayField("pickaxes"))
		{
			Pickaxes.Add(FPickaxeData(Pickaxe->AsObject()));
		}
		
		for(auto& Ore : JsonObject->GetArrayField("resources"))
		{
			auto OreObject = Ore->AsObject();
			Ores.Add((EOre)OreObject->GetIntegerField("resource_id"), OreObject->GetIntegerField("num_of"));
		}

	}
};

DECLARE_DYNAMIC_DELEGATE_TwoParams(FGetPlayerDataCallback, FClientPlayerData, PlayerData, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FAddPlayerCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FSetResourceCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FPostListingCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FClaimListingCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FAddPickaxeCallback, int, AxeId, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FRemovePickaxeCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_OneParam(FPurchaseCallback, bool, bWasSuccessful);
DECLARE_DYNAMIC_DELEGATE_TwoParams(FGetListingsCallback, TArray<FExchangeListing> const&, ExchangeListings, bool, bWasSuccessful);

UCLASS()
class GMTK19_API UPickaxeExchangeRequestWrapper : public UObject
{
	GENERATED_BODY()

public:
	FGetPlayerDataCallback GetPlayerDataCallback;
	FAddPlayerCallback AddPlayerCallback;
	FSetResourceCallback SetResourceCallback;
	FPostListingCallback PostListingCallback;
	FClaimListingCallback ClaimListingCallback;
	FAddPickaxeCallback AddPickaxeCallback;
	FRemovePickaxeCallback RemovePickaxeCallback;
	FPurchaseCallback PurchaseCallback;
	FGetListingsCallback GetListingsCallback;

	void GetPlayerDataResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void AddPlayerResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void SetResourceResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void PostListingResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void ClaimListingResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void AddPickaxeResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void RemovePickaxeResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void PurchaseResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	void GetListingsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
	bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);
};

UCLASS(BlueprintType, Blueprintable)
class GMTK19_API APickaxeExchangeClient : public AActor
{
	GENERATED_BODY()

    FString LocalHost = "http://localhost:8011/";
    FString RemoteHost = "http://localhost:8011/";

	bool bUseLocal = true;

	FHttpModule* Http;

    TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
	void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);

	TSharedRef<IHttpRequest> GetRequest(FString Subroute, const TMap<FString, FString>& QueryParams);
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
	void Send(TSharedRef<IHttpRequest>& Request);

	bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString HostOverride;

	APickaxeExchangeClient();
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void GetPlayerData(FString Name, const FGetPlayerDataCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void AddPlayer(FClientPlayerInfo NewPlayer, const FAddPlayerCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void SetResource(FResourceRequest SetResourceRequest, const FSetResourceCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void PostListing(FPostListingRequest PostListingRequest, const FPostListingCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void ClaimListing(FClaimListingRequest ClaimListingRequest, const FClaimListingCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void RemoveListing(FClaimListingRequest RemoveListingRequest, const FClaimListingCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void RemovePickaxe(FRemovePickaxeRequest RemovePickaxeRequest, const FRemovePickaxeCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void Purchase(FPurchaseRequest PurchaseRequest, const FPurchaseCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void GetListings(FGetListingRequest GetListingsRequest, const FGetListingsCallback& Callback);

	UFUNCTION(BlueprintCallable)
	void AddPickaxe(FAddPickaxeRequest AddPickaxeRequest, const FAddPickaxeCallback& Callback);
};