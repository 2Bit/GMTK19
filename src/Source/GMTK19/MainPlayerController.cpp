// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "MainPlayerController.h"

#include "UI/MenuHUD.h"
#include "UI/BattleHUD.h"
#include "UI/BreedingHUD.h"
#include "UI/ResearchHUD.h"
#include "UI/MarketplaceHUD.h"
#include "UI/InventoryWidget.h"
#include "MainGameState.h"
#include "Inventory.h"
#include "Pickaxe.h"

AMainPlayerController::AMainPlayerController()
{
	this->ViewTransitionParams.BlendTime = 2.0f;
	this->ViewTransitionParams.BlendFunction = EViewTargetBlendFunction::VTBlend_EaseInOut;
	this->ViewTransitionParams.BlendExp = 2.0f;
	this->ViewTransitionParams.bLockOutgoing = true;

	this->bShowMouseCursor = true;
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	this->SetInputMode(FInputModeUIOnly());

	if (this->MenuHUDClass != nullptr)
	{
		this->MenuHUD = CreateWidget<UMenuHUD>(this, this->MenuHUDClass);
		this->MenuHUD->AddToViewport();
	}

	if (this->BattleHUDClass != nullptr)
	{
		this->BattleHUD = CreateWidget<UBattleHUD>(this, this->BattleHUDClass);
		this->BattleHUD->AddToViewport();
	}

	if (this->BreedingHUDClass != nullptr)
	{
		AMainGameState* const MainGameState = Cast<AMainGameState>(this->GetWorld()->GetGameState());
		if (MainGameState != nullptr)
		{
			this->BreedingHUD = CreateWidget<UBreedingHUD>(this, this->BreedingHUDClass);
			this->BreedingHUD->AddToViewport();
			this->BreedingHUD->Configure(MainGameState->GetPlayerInventory());
		}
	}

	if (this->ResearchHUDClass != nullptr)
	{
		this->ResearchHUD = CreateWidget<UResearchHUD>(this, this->ResearchHUDClass);
		this->ResearchHUD->AddToViewport();
	}

	if (this->MarketplaceHUDClass != nullptr)
	{
		this->MarketplaceHUD = CreateWidget<UMarketplaceHUD>(this, this->MarketplaceHUDClass);
		this->MarketplaceHUD->AddToViewport();
	}

	if (this->InventoryWidgetClass != nullptr)
	{
		AMainGameState* const MainGameState = Cast<AMainGameState>(this->GetWorld()->GetGameState());
		if (MainGameState != nullptr)
		{
			this->InventoryWidget = CreateWidget<UInventoryWidget>(this, this->InventoryWidgetClass);
			this->InventoryWidget->AddToViewport();
			this->InventoryWidget->Configure(MainGameState->GetPlayerInventory());
			
			MainGameState->GetPlayerInventory()->OnInventoryChanged.AddDynamic(this->InventoryWidget, &UInventoryWidget::Refresh);
		}
	}

	this->ReturnToMenu();
}

void AMainPlayerController::SetMenuView(class AActor* NewMenuView)
{
	this->MenuView = NewMenuView;
}

void AMainPlayerController::ReturnToMenu()
{
	if (this->BattleHUD != nullptr)
	{
		this->BattleHUD->Hide();
	}
	
	if (this->BreedingHUD != nullptr)
	{
		this->BreedingHUD->Hide();
	}

	if (this->ResearchHUD != nullptr)
	{
		this->ResearchHUD->Hide();
	}

	if (this->MarketplaceHUD != nullptr)
	{
		this->MarketplaceHUD->Hide();
	}

	if (this->MenuHUD != nullptr)
	{
		this->MenuHUD->Show();
	}

	this->ClientSetViewTarget(this->MenuView, ViewTransitionParams);
}

void AMainPlayerController::SetBattleView(class AActor* NewBattleView)
{
	this->BattleView = NewBattleView;
}

bool AMainPlayerController::IsBattling() const
{
	return this->BattleHUD != nullptr && this->BattleHUD->IsShowing();
}

void AMainPlayerController::BeginBattle(UPickaxe* Enemy)
{
	if (this->BattleHUD != nullptr && !this->BattleHUD->IsShowing())
	{
		AMainGameState* const MainGameState = Cast<AMainGameState>(this->GetWorld()->GetGameState());
		if (MainGameState != nullptr && MainGameState->GetPlayerInventory()->GetPickaxeCount() > 0)
		{
			if (this->MenuHUD != nullptr)
			{
				this->MenuHUD->Hide();
			}

			this->BattleHUD->Configure(MainGameState->GetPlayerInventory()->RemovePickaxeAt(0), Enemy);
			this->BattleHUD->Show();

			this->ClientSetViewTarget(this->BattleView, ViewTransitionParams);
		}
	}
}

bool AMainPlayerController::IsBreeding() const
{
	return this->BreedingHUD != nullptr && this->BreedingHUD->IsShowing();
}

void AMainPlayerController::BeginBreeding()
{
	if (this->BreedingHUD != nullptr && !this->BreedingHUD->IsShowing())
	{
		if (this->MenuHUD != nullptr)
		{
			this->MenuHUD->Hide();
		}

		this->BreedingHUD->Show();
	}
}

bool AMainPlayerController::IsResearching() const
{
	return this->ResearchHUD != nullptr && this->ResearchHUD->IsShowing();
}

void AMainPlayerController::BeginResearching()
{
	if (this->ResearchHUD != nullptr && !this->ResearchHUD->IsShowing())
	{
		if (this->MenuHUD != nullptr)
		{
			this->MenuHUD->Hide();
		}

		this->ResearchHUD->Show();
	}
}

bool AMainPlayerController::IsTrading() const
{
	return this->MarketplaceHUD != nullptr && this->MarketplaceHUD->IsShowing();
}

void AMainPlayerController::BeginTrading()
{
	if (this->MarketplaceHUD != nullptr && !this->MarketplaceHUD->IsShowing())
	{
		if (this->MenuHUD != nullptr)
		{
			this->MenuHUD->Hide();
		}

		this->MarketplaceHUD->Show();
	}
}

bool AMainPlayerController::IsShowingInventory() const
{
	return this->InventoryWidget != nullptr && this->InventoryWidget->IsShowing();
}

void AMainPlayerController::ShowInventory(FString const& RetrieveText, bool Selectable)
{
	if (this->InventoryWidget != nullptr && !this->IsShowingInventory())
	{
		this->InventoryWidget->Show(RetrieveText, Selectable);
	}
}

void AMainPlayerController::HideInventory()
{
	if (this->IsShowingInventory())
	{
		this->InventoryWidget->Hide();
	}
}
