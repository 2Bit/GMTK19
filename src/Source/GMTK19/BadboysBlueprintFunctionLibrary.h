// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BadboysBlueprintFunctionLibrary.generated.h"

/**
 * Cool tricks for bad boys.
 */
UCLASS()
class GMTK19_API UBadboysBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category = "Sick Tricks")
	static int32 GetUniqueID(const UObject* Object);

};
