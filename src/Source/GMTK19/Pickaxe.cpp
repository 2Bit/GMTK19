// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "Pickaxe.h"

void UPickaxe::SetData(FPickaxeData const& NewData)
{
	this->Data = NewData;

	this->HitPoints = this->Data.Durability.Value;
}

void UPickaxe::ModifyHitPoints(int32 DeltaHitPoints)
{
	if (!this->IsDead())
	{
		this->HitPoints += FMath::Min(DeltaHitPoints, (int)this->Data.Durability.Value - this->HitPoints);

		if (this->HitPoints <= 0)
		{
			this->bIsDead = true;
		}

		this->OnStatusChange.Broadcast(this);
	}
}

void UPickaxe::BeginDefending()
{
	if (!this->bIsDefending)
	{
		this->bIsDefending = true;

		this->OnStatusChange.Broadcast(this);
	}
}

void UPickaxe::EndDefending()
{
	if (this->bIsDefending)
	{
		this->bIsDefending = false;

		this->OnStatusChange.Broadcast(this);
	}
}

int32 UPickaxe::CalculateOutgoingDamage(EPickaxeAction Action) const
{
	if (this->IsDead())
	{
		return 0;
	}

	return UPickaxeActionHelper::GetDetails(Action).Damage * this->Data.Attack.Value;
}

int32 UPickaxe::CalculateIncomingDamage(int32 RawDamage) const
{
	if (this->IsDead())
	{
		return 0;
	}

	if (this->IsDefending())
	{
		return FMath::Min(1, RawDamage);
	}

	return FMath::Max(RawDamage / 2, RawDamage - (int)this->Data.Defense.Value);
}
