// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "PickaxeAction.generated.h"

UENUM(BlueprintType)
enum class EPickaxeAction : uint8
{
	None = 0,
	Smash = 1,
	Pry = 2,
	MegaSmash = 3,
	Whack = 4,
	Slam = 5,
	Poke = 6,
	Swipe = 7,
	MAX = 8,
};

USTRUCT(BlueprintType)
struct FPickaxeActionDetails
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Accuracy;
};

UCLASS(Abstract)
class UPickaxeActionHelper : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Actions)
	static FPickaxeActionDetails GetDetails(EPickaxeAction Action);
	static TArray<EPickaxeAction> CommonPool;
	static TArray<EPickaxeAction> RarePool;
};
