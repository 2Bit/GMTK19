// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "OreStruct.h"

#include "Inventory.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FInventoryChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRetrievePickaxe, class UPickaxe*, Pickaxe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPickaxeAdded, class UPickaxe*, Pickaxe);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPickaxeRemoved, int, PickaxeID);

UCLASS(BlueprintType)
class UInventory : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = Data)
	FPickaxeAdded OnPickaxeAdded;

	UPROPERTY(BlueprintAssignable, Category = Data)
	FPickaxeRemoved OnPickaxeRemoved;

	UPROPERTY(BlueprintAssignable, Category = Data)
	FInventoryChanged OnInventoryChanged;

	UPROPERTY(BlueprintAssignable, Category = Data)
	FRetrievePickaxe OnRetrievePickaxe;

	UFUNCTION(BlueprintCallable, Category = Data)
	int32 GetPickaxeCount() const;

	UFUNCTION(BlueprintCallable, Category = Data)
	class UPickaxe* GetPickaxe(int32 Slot);

	UFUNCTION(BlueprintCallable, Category = Data)
	bool AddPickaxe(class UPickaxe* NewPickaxe);

	UFUNCTION(BlueprintCallable, Category = Data)
	class UPickaxe* RemovePickaxeAt(int32 Slot);

	UFUNCTION(BlueprintCallable, Category = Data)
	bool RemovePickaxe(UPickaxe* axe);

	UFUNCTION(BlueprintCallable, Category = Data)
	bool RetrievePickaxe(int32 Slot);

	UFUNCTION(BlueprintCallable, Category = Data)
	bool ModifyOreAmount(const FOreStruct& Value, bool IsPositive = true);
	
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Data)
	FOreStruct GetCurrentOres(){return this->Ores;}

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = true))
	TArray<class UPickaxe*> Pickaxes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = true))
	FOreStruct Ores;

	bool IsLegitimateTransaction(const FOreStruct& Value);
};
