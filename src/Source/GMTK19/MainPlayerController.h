// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "MainPlayerController.generated.h"

UCLASS()
class AMainPlayerController : public APlayerController
{
	GENERATED_BODY()

	AMainPlayerController();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = HUD)
	void SetMenuView(class AActor* NewMenuView);

	UFUNCTION(BlueprintCallable, Category = HUD)
	void ReturnToMenu();

	UFUNCTION(BlueprintCallable, Category = HUD)
	void SetBattleView(class AActor* NewBattleView);

	UFUNCTION(BlueprintCallable, Category = HUD)
	bool IsBattling() const;

	UFUNCTION(BlueprintCallable, Category = HUD)
	void BeginBattle(class UPickaxe* Enemy);

	UFUNCTION(BlueprintCallable, Category = HUD)
	bool IsBreeding() const;

	UFUNCTION(BlueprintCallable, Category = HUD)
	void BeginBreeding();

	UFUNCTION(BlueprintCallable, Category = HUD)
	bool IsResearching() const;

	UFUNCTION(BlueprintCallable, Category = HUD)
	void BeginResearching();

	UFUNCTION(BlueprintCallable, Category = HUD)
	bool IsTrading() const;

	UFUNCTION(BlueprintCallable, Category = HUD)
	void BeginTrading();

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool IsShowingInventory() const;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void ShowInventory(FString const& RetrieveText, bool Selectable = false);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void HideInventory();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD, meta = (AllowPrivateAccess = true))
	FViewTargetTransitionParams ViewTransitionParams;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class AActor* MenuView;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UMenuHUD> MenuHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class UMenuHUD* MenuHUD;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class AActor* BattleView;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UBattleHUD> BattleHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class UBattleHUD* BattleHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UBreedingHUD> BreedingHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class UBreedingHUD* BreedingHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UResearchHUD> ResearchHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class UResearchHUD* ResearchHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UMarketplaceHUD> MarketplaceHUDClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUD, meta = (AllowPrivateAccess = true))
	class UMarketplaceHUD* MarketplaceHUD;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = true))
	TSubclassOf<class UInventoryWidget> InventoryWidgetClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = true))
	class UInventoryWidget* InventoryWidget;

public:
	FORCEINLINE class UMenuHUD* const GetMenuHUD() const { return this->MenuHUD; }
	FORCEINLINE class UBattleHUD* const GetBattleHUD() const { return this->BattleHUD; }
	FORCEINLINE class UBreedingHUD* const GetBreedingHUD() const { return this->BreedingHUD; }
	FORCEINLINE class UResearchHUD* const GetResearchHUD() const { return this->ResearchHUD; }
	FORCEINLINE class UMarketplaceHUD* const GetMarketplaceHUD() const { return this->MarketplaceHUD; }
};
