// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Ore.generated.h"

UENUM(BlueprintType)
enum class EOre : uint8
{
	Gold = 0,
	Tungsten = 1,
	Diamond = 2,
	Iron = 3,
};

USTRUCT(BlueprintType)
struct FOreDetails
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Description;
};

UCLASS(Abstract)
class UOreHelper : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Ore)
	static FOreDetails GetDetails(EOre Ore);
};
