// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "Ore.h"

FOreDetails UOreHelper::GetDetails(EOre Ore)
{
	FOreDetails Details;
	Details.Name = "None";
	Details.Description = "Congratulations, this bug is actually an easter egg so don't report it to us.";

	switch (Ore)
	{
	case EOre::Gold:
		Details.Name = "Gold";
		Details.Description = "NEEDS COPY FROM HELEN";

		break;
	case EOre::Tungsten:
		Details.Name = "Tungsten";
		Details.Description = "NEEDS COPY FROM HELEN";

		break;
	case EOre::Diamond:
		Details.Name = "Diamond";
		Details.Description = "NEEDS COPY FROM HELEN";

		break;
	case EOre::Iron:
		Details.Name = "Iron";
		Details.Description = "NEEDS COPY FROM HELEN";

		break;
	}

	return Details;
}
