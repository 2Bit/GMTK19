// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Json.h"
#include "JsonUtilities.h"
#include "PickaxeData.h"
#include "Ore.h"

#include "ExchangeListing.generated.h"

USTRUCT(BlueprintType)
struct FExchangeListing
{
	GENERATED_BODY()

	FExchangeListing() {}
	FExchangeListing(TSharedPtr<FJsonObject> JsonObject)
	{
		ID = JsonObject->GetIntegerField("listing_id");

        if(JsonObject->HasField("axe_id"))
        {
            Pickaxe = FPickaxeData(JsonObject);
            ListedResourceType = (EOre)0;
            ListedResourceNum = 0;
        } else 
        {
            ListedResourceType = (EOre)JsonObject->GetIntegerField("listed_resource");
            ListedResourceNum = JsonObject->GetIntegerField("listed_resource_num");
        }

        BuyResourceType = (EOre)JsonObject->GetIntegerField("buy_resource");
        BuyResourceNum = JsonObject->GetIntegerField("buy_resource_num");
	}

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	int ID;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	FPickaxeData Pickaxe;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	EOre ListedResourceType;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	int ListedResourceNum;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	EOre BuyResourceType;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Persona)
	int BuyResourceNum;
};