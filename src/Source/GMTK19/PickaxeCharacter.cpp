// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "PickaxeCharacter.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Camera/CameraShake.h"
#include "Components/SkeletalMeshComponent.h"
#include "Pickaxe.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/GameplayStatics.h"

static float ATTACK_TIME_HIT = 0.5f;
static float ATTACK_TIME_PREP = 0.7f;
static float ATTACK_TIME_MAX = 1.1f;
static float RECOIL_TIME_FREEZE = 0.2f;
static float RECOIL_TIME_MAX = 0.4f;

APickaxeCharacter::APickaxeCharacter()
{
	this->PrimaryActorTick.bCanEverTick = true;
	this->PrimaryActorTick.bStartWithTickEnabled = true;
}

void APickaxeCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->Randomness = FMath::FRand();
}

void APickaxeCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float const RealTimeSeconds = this->GetWorld()->GetRealTimeSeconds() + this->Randomness * 20.0f;

	float Weight = FMath::Sin(RealTimeSeconds * 2.0f);
	Weight = (FMath::Log2(1.0f + FMath::Abs(Weight) * 3.0f) * 0.5f) * FMath::Sign(Weight);

	FVector TargetLocation(0.0f, Weight * 10.0f, -40.0f);
	FRotator TargetRotation(-30.0f, Weight * -10.0f, 0.0f);

	if (this->GetVelocity().SizeSquared() > 50.0f * 50.0f)
	{
		Weight = FMath::Sin(RealTimeSeconds * 15.0f);
		Weight = (FMath::Log2(1.0f + FMath::Abs(Weight) * 3.0f) * 0.5f) * FMath::Sign(Weight);

		TargetLocation += FVector(0.0f, 0.0f, Weight * 20.0f);
	}

	if (this->AttackTimeRemaining > 0.0f)
	{
		TargetLocation = FVector::ZeroVector;
		TargetRotation = FRotator::ZeroRotator;

		this->AttackTimeRemaining = FMath::Max(0.0f, this->AttackTimeRemaining - DeltaSeconds);

		Weight = FMath::Clamp((this->AttackTimeRemaining - ATTACK_TIME_HIT) / (ATTACK_TIME_MAX - ATTACK_TIME_HIT), 0.0f, 1.0f);

		TargetLocation += FVector(0.0f, Weight * (-150.0f + this->AttackRandomness * 300.0f), 0.0f);

		if (this->AttackTimeRemaining > ATTACK_TIME_PREP)
		{
			float const TimeTilSwing = this->AttackTimeRemaining - ATTACK_TIME_PREP;

			Weight = FMath::Pow(TimeTilSwing / (ATTACK_TIME_MAX - ATTACK_TIME_PREP), 3.0f);

			TargetLocation += FVector(Weight * 100.0f, 0.0f, Weight * 100.0f);
			TargetRotation += FRotator(120.0f * Weight, 0.0f, 0.0f);
		}
		else if (this->AttackTimeRemaining > ATTACK_TIME_HIT)
		{
			float const TimeTilHit = this->AttackTimeRemaining - ATTACK_TIME_HIT;

			Weight = 1.0f - TimeTilHit / (ATTACK_TIME_MAX - ATTACK_TIME_HIT);

			TargetLocation += this->GetActorRotation().UnrotateVector((this->AttackTarget->GetActorLocation() - this->GetActorLocation()) * Weight);

			if (this->AttackRandomness > 0.2f)
			{
				TargetRotation += FRotator(-150.0f * FMath::Pow(Weight, 5.0f), 0.0f, 0.0f);
			}
			else
			{
				TargetRotation += FRotator(90.0f, 0.0f, 0.0f);
			}
		}
		else
		{
			if (this->AttackTimeRemaining + DeltaSeconds > ATTACK_TIME_HIT)
			{
				if (this->bDoesAttackHit)
				{
					this->AttackTarget->PlayHurt();

					UGameplayStatics::PlaySound2D(this, this->HitSound);

					this->GetWorld()->GetFirstPlayerController()->PlayerCameraManager->PlayCameraShake(this->HitShakeClass);
				}
				else
				{
					UGameplayStatics::PlaySound2D(this, this->MissSound);
				}
			}

			Weight = this->AttackTimeRemaining / ATTACK_TIME_HIT;

			TargetLocation += this->GetActorRotation().UnrotateVector((this->AttackTarget->GetActorLocation() - this->GetActorLocation()) * Weight);
		}
	}

	if (this->RecoilTimeRemaining > 0.0f)
	{
		this->RecoilTimeRemaining = FMath::Max(0.0f, this->RecoilTimeRemaining - DeltaSeconds);

		if (this->RecoilTimeRemaining > RECOIL_TIME_FREEZE)
		{
			float const TimeTilKickback = this->RecoilTimeRemaining - RECOIL_TIME_FREEZE;

			TargetLocation += FVector(FMath::Sin(TimeTilKickback * 50.0f) * 80.0f, FMath::Cos(TimeTilKickback * 75.0f) * 80.0f, 0.0f);
		}
		else
		{
			Weight = (1.0f - FMath::Pow(this->RecoilTimeRemaining / RECOIL_TIME_MAX, 8.0f));

			TargetRotation += FRotator(Weight * 70.0f, -50 + Weight * 180.0f, 0.0f);
		}
	}

	this->GetMesh()->SetRelativeLocationAndRotation(
		FMath::Lerp(this->GetMesh()->RelativeLocation, TargetLocation, DeltaSeconds * 10.0f),
		FMath::Lerp(this->GetMesh()->RelativeRotation, TargetRotation, DeltaSeconds * 10.0f));
}

void APickaxeCharacter::Configure(UPickaxe* NewTarget)
{
	this->Target = NewTarget;

	if (this->BodyMaterial == nullptr)
	{
		this->BodyMaterial = this->GetMesh()->CreateDynamicMaterialInstance(1);
	}

	if (this->HeadMaterial == nullptr)
	{
		this->HeadMaterial = this->GetMesh()->CreateDynamicMaterialInstance(0);
	}

	if (this->Target != nullptr)
	{
		this->HeadMaterial->SetVectorParameterValue("Colour", this->Target->GetData().Colour);
	}
}

void APickaxeCharacter::PlayAttack(APickaxeCharacter* NewAttackTarget, bool DoesAttackHit)
{
	this->AttackTarget = NewAttackTarget;
	this->AttackTimeRemaining = ATTACK_TIME_MAX;
	this->AttackRandomness = FMath::FRand();
	this->bDoesAttackHit = DoesAttackHit;
}

void APickaxeCharacter::PlayHurt()
{
	this->RecoilTimeRemaining = RECOIL_TIME_MAX;

	this->HeadMaterial->SetScalarParameterValue("RecoilTime", this->GetWorld()->RealTimeSeconds);
	this->BodyMaterial->SetScalarParameterValue("RecoilTime", this->GetWorld()->RealTimeSeconds);
}
