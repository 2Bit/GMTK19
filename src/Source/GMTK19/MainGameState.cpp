// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "MainGameState.h"

#include "Inventory.h"
#include "Pickaxe.h"
#include "BreedingBlueprintFunctionLibrary.h"

void AMainGameState::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	this->PlayerInventory = NewObject<UInventory>(this);

	/// temporary pickaxe config
	FPickaxeData PickaxeData1;
	PickaxeData1.Yield.Value = 3;
	PickaxeData1.Durability.Value = 26;
	PickaxeData1.Attack.Value = 12;
	PickaxeData1.Defense.Value = 12;
	PickaxeData1.Action1 = EPickaxeAction::Smash;
	PickaxeData1.Action2 = EPickaxeAction::Pry;
	PickaxeData1.Name = UBreedingBlueprintFunctionLibrary::GetName(PickaxeData1);

	UPickaxe* Pickaxe1 = NewObject<UPickaxe>(this);
	Pickaxe1->SetData(PickaxeData1);

	FPickaxeData PickaxeData2;
	PickaxeData2.Yield.Value = 2;
	PickaxeData2.Durability.Value = 21;
	PickaxeData2.Attack.Value = 14;
	PickaxeData2.Defense.Value = 10;
	PickaxeData2.Action1 = EPickaxeAction::Smash;
	PickaxeData2.Action2 = EPickaxeAction::Pry;
	PickaxeData2.Name = UBreedingBlueprintFunctionLibrary::GetName(PickaxeData2);

	UPickaxe* Pickaxe2 = NewObject<UPickaxe>(this);
	Pickaxe2->SetData(PickaxeData2);

	this->GetPlayerInventory()->ModifyOreAmount({0,0,0,0});

	this->GetPlayerInventory()->AddPickaxe(Pickaxe1);
	this->GetPlayerInventory()->AddPickaxe(Pickaxe2);
	///
}
