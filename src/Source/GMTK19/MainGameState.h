// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"

#include "Ore.h"

#include "MainGameState.generated.h"

UCLASS()
class AMainGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	virtual void PostInitializeComponents() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Data, meta = (AllowPrivateAccess = true))
	EOre AvailableOre;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Data, meta = (AllowPrivateAccess = true))
	int32 DifficultyProgress;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Data, meta = (AllowPrivateAccess = true))
	int32 DiscoveryProgress;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Data, meta = (AllowPrivateAccess = true))
	class UInventory* PlayerInventory;

public:
	FORCEINLINE class UInventory* const GetPlayerInventory() const { return this->PlayerInventory; }
};
