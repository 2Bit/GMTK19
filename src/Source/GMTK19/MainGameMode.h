// Copyright 2019 2Bit Studios, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "MainGameMode.generated.h"

UCLASS()
class AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

	AMainGameMode();

public:
	virtual void Tick(float DeltaSeconds) override;

private:
	UPROPERTY()
	float AIActionDelay;
};
