// Copyright 2019 2Bit Studios, All Rights Reserved.

#include "MainGameMode.h"

#include "MainPlayerController.h"
#include "MainGameState.h"
#include "Engine/World.h"
#include "UI/BattleHUD.h"
#include "Pickaxe.h"
#include "PickaxeAction.h"

AMainGameMode::AMainGameMode()
{
	this->PlayerControllerClass = AMainPlayerController::StaticClass();
	this->GameStateClass = AMainGameState::StaticClass();

	this->PrimaryActorTick.bCanEverTick = true;
	this->PrimaryActorTick.bStartWithTickEnabled = true;
}

void AMainGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AMainPlayerController* const MainPlayerController = Cast<AMainPlayerController>(this->GetWorld()->GetFirstPlayerController());
	if (MainPlayerController != nullptr)
	{
		if (MainPlayerController->IsBattling())
		{
			UBattleHUD* const BattleHUD = MainPlayerController->GetBattleHUD();
			if (BattleHUD->IsEnemyTurn() && !BattleHUD->GetEnemy()->IsDead())
			{
				this->AIActionDelay -= DeltaSeconds;
				if (this->AIActionDelay <= 0.0f)
				{
					FPickaxeData const EnemyData = BattleHUD->GetEnemy()->GetData();
					if (BattleHUD->PerformAction(BattleHUD->GetEnemy(), BattleHUD->GetPlayer(), FMath::RandBool() ? EnemyData.Action1 : EnemyData.Action2))
					{
						BattleHUD->EndEnemyTurn();
					}
				}
			}
			else
			{
				this->AIActionDelay = 3.0f;
			}
		}
	}
}
